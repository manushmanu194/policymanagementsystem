package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Policy {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String policyType;

	private String policyName;

	private String companyName;
	private Integer years;
	private Integer price;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPolicyType() {
		return policyType;
	}
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}
	public String getPolicyName() {
		return policyName;
	}
	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Integer getYears() {
		return years;
	}
	public void setYears(Integer years) {
		this.years = years;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public Policy(int id, String policyType, String policyName, String companyName, Integer years, Integer price) {
		super();
		this.id = id;
		this.policyType = policyType;
		this.policyName = policyName;
		this.companyName = companyName;
		this.years = years;
		this.price = price;
	}
	public Policy() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Policy [id=" + id + ", policyType=" + policyType + ", policyName=" + policyName + ", companyName="
				+ companyName + ", years=" + years + ", price=" + price + "]";
	}
	
}
