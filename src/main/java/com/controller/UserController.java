package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.dao.UserDAO;
import com.dao.UserDAOImpl;
import com.model.User;


@Controller
public class UserController {
	UserDAO dao= new UserDAOImpl();
	@RequestMapping("userRegister")
	public ModelAndView  userRegister() {
	return new ModelAndView("userRegister","u",new  User());
	}
	@RequestMapping("userAdd")
	public ModelAndView add(@ModelAttribute("u")  User u) {
		dao.insertUser(u);
		return new ModelAndView("display");
	}
@RequestMapping("userLogin")
public ModelAndView  userLogin()
{
	return new ModelAndView("userLogin","u",new  User());
}
    @RequestMapping("userValidate")
    public ModelAndView  userValidate(@ModelAttribute("u")  User user)
{
    	boolean isValid = dao. userAuthentication( user);
    	if(isValid) {
    	return new ModelAndView("userLogin", "v", "Login Successfully");
    	} else {
    		return new ModelAndView("userLogin", "v", "Check your Credentials");
    	}
}
}
