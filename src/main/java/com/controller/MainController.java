package com.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;

import com.dao.PolicyDao;
import com.model.Policy;

@Controller
public class MainController {
	@Autowired
	private PolicyDao policyDao;
	
	@RequestMapping("/")
	public String home(Model m) {
		
		List<Policy> policies =policyDao.getPolicies();
		m.addAttribute("policies", policies);
		
		return "index";
	}
	@RequestMapping("/add-policy")
	public String addPolicy(Model m) {
		m.addAttribute("title", "Add Policy");
		return "add_policy_formm";
	}
		//handle add product form
	
	@RequestMapping(value="/handle-policy",method= RequestMethod.POST)
	public RedirectView handlePolicy(@ModelAttribute Policy policy,HttpServletRequest request) {
		System.out.println(policy);
		policyDao.createPolicy(policy);
		RedirectView redirectView=new RedirectView();
		redirectView.setUrl(request.getContextPath()+"/");
		return redirectView;
	}
	
	//delete handler
	@RequestMapping("/delete/{policyId}")
	public RedirectView deletePolicy(@PathVariable("policyId") int policyId,HttpServletRequest request) {
		this.policyDao.deletePolicy(policyId); 
		RedirectView redirectView=new RedirectView();
		redirectView.setUrl(request.getContextPath()+"/");
		return redirectView;
		
	}
	// update handler
	
	@RequestMapping("/update/{policyId}")
public String updateForm(@PathVariable("policyId")int pid,Model model) {
	
	 Policy policy =  this.policyDao.getPolicy(pid);
		model.addAttribute("policy", policy);
		return "update_form";
}
	
}
