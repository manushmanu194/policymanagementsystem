<html>
<head>
<%@include file="./base.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

</head>

<body>
	<div class="container mt-3">
		<div class="row">
			<div class="col-md-12">
				<h1 class="text-center mb-3">Welcome to Policy Management
					System</h1>
				<table class="table caption-top">
					<caption>List of users</caption>
					<thead class="table-dark">
						<tr>
							<th scope="col">Policy Id</th>
							<th scope="col">Policy Type</th>
							<th scope="col">Policy Name</th>
							<th scope="col">Company Name</th>
							<th scope="col">Policy Duration</th>
							<th scope="col">Policy Price</th>
							<th scope="col">Action</th>


						</tr>
					</thead>
					<tbody>
					
					<c:forEach items="${policies }" var="p">
						<tr>
							<th scope="row">POLCIES${p.id}</th>
							<td>${p.policyType}</td>
							<td>${p.policyName }</td>
							<td>${p.companyName }</td>
							<td>${p.years }</td>
							<td>&#x20B9;${p.price }</td>
							<td>
							<a href="delete/${p.id }"><i class="fas fa-trash-danger" style="font-size: 30px;"></i></a>
							<a href="update/${p.id }"><i class="fas fa-pen-nib-primary" style="font-size: 30px;"></i></a>
						
						</td>
						</tr>
						</c:forEach>
						
					</tbody>
				</table>
				<div class="container text-center">
					<a href="add-policy" class="btn btn-outline-success">Add Policy</a>

				</div>

			</div>

		</div>
	</div>
</body>
</html>
